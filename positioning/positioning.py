import cv2
import numpy as np
import math
from mapping import mapping
from mapping import car_calibrate
import numpy.linalg as la
#import paho.mqtt.publish as publish
from xbee import XBee
import serial
import time 

def caller_position(frame):
   # blurred = cv2.GaussianBlur(frame, (11, 11), 0)
    img = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    #cv2.imshow("hsv", img)
    #cv2.waitKey(1)
    greenlow = (40, 40, 200)
    greenhigh = (70, 120, 255)
    mask = cv2.inRange(img, greenlow, greenhigh)
    mask = cv2.erode(mask, None, iterations = 2)
    mask = cv2.dilate(mask, None, iterations = 2)
    im_caller, cnts, h = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(cnts) > 0:
        c = max(cnts, key = cv2.contourArea)
        M = cv2.moments(c)
        center = (int(M["m10"] / M["m00"]), int((M["m01"] / M["m00"])))
        cv2.circle(frame, center, 10, (1, 227, 254), -1)
        return center
    else: 
        no_object = []
        return no_object
'''
def car_position(frame):
    #blurred = cv2.GaussianBlur(frame, (11, 11), 0)
    img = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    bluelow = (80, 70, 160)
    bluehigh = (130, 130, 255)
    mask = cv2.inRange(img, bluelow, bluehigh)
    #cv2.imshow("mask", mask)
    #cv2.waitKey(1)
    mask = cv2.erode(mask, None, iterations = 2)
    mask = cv2.dilate(mask, None, iterations = 2)
    im_taxi, cnts, h = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(cnts) > 0:
        car_position = np.zeros((len(cnts), 2), dtype = np.int)
        k = 0
        for i in cnts:
            M = cv2.moments(i)
            if M["m00"] != 0:
                center = [int(M["m10"] / M["m00"]), int((M["m01"] / M["m00"]))]
                ratio_h = 1 - center[0]/640
                ratio_v = 1 - center[1]/480
                print(ratio_h, ratio_v)
                print("Untuned center: ", center)
                car_position[k][0] = center[0]
                car_position[k][1] = center[1]
                if (center[0] > 580):
                    car_position[k][0] = center[0] - ratio_h * center[0] * 0.6
                elif (center[0] <= 580 and center[0] > 350):
                    car_position[k][0] = center[0] - ratio_h * center[0] * 0.15
                elif(center[0] <= 350 and center[0] >= 290 and center[1] > 80 and center[1] <=400):
                    car_position[k][0] = center[0] - ratio_h * center[0] * 0.01
                    car_position[k][1] = center[1] - ratio_h * center[1] * 0.35
                elif(center[0] <= 300 and center[0] >= 65): #240
                     car_position[k][0] = center[0] - ratio_h * center[0] * 0.05
                elif(center[0] >= 40 and center[0] < 65):
                     car_position[k][0] = center[0] + ratio_h * center[0] * 0.4 
                elif(center[0] < 40):
                     car_position[k][0] = center[0] + ratio_h * center[0] * 0.75
                if (center[1] > 420 and center[1] < 460):
                    car_position[k][1] = center[1] - ratio_v * center[1] * 1.6 
                elif(center[1] >= 460):
                    car_position[k][1] = center[1] - ratio_v * center[1] * 4.2 
                elif(center[1] > 320 and center[1] <= 420):
                    car_position[k][1] = center[1] - ratio_v * center[1] * 0.4
                elif(center[1] <= 320 and center[0] > 350 or center[0] < 300):
                    car_position[k][1] = center[1] - ratio_v * center[1] * 0.5

                cv2.circle(frame, (car_position[k][0], car_position[k][1]), 8, (255, 255, 0), -1)
                k = k + 1
        car_position = np.array(car_position)
        
        #cv2.imshow("car", frame)
        #cv2.waitKey(1)
        return car_position
    else:
        no_object = [] 
        return no_object
'''    
def find_closest(customer, taxi):
    distance = 100000000;
    k = -1
    for i in taxi:
        dis = math.sqrt((i[0] - customer[0]) ** 2 + (i[1] - customer[1]) **2)
        k = k + 1
        if dis < distance:
            index = k
            distance = dis
            chosen_taxi = i
    
    #print("The closest distance:", distance)

    return chosen_taxi, index

def path_decide(source, destination):
    graph = [[0, 2, 1, 3, 0], [3, 0, 2, 0, 2], [1, 1, 0, 2, 1], [2, 0, 2, 0, 3], [0, 3, 1, 2 ,0]]
    spot_pass = np.zeros((5,1), dtype = np.int)
    dist = np.ones((5,), dtype = np.int)*100
    path = np.ones((5,), dtype = np.int)*-1
    path[0] = -1
    dist[source] = 0
    node_choose = []
    
    for v in range(0,4):
        u = minDistance(dist, spot_pass)
        spot_pass[u] = 1
        for i in range(0, 5):
            if (spot_pass[i] == 0 and graph[u][i] and dist[u] + graph[u][i] < dist[i]):
                path[i] = u
                dist[i] = dist[u] + graph[u][i]
    route = []
    route.append(source)
    path_get(path, destination, route)
    return route
    

def minDistance(dist, spot_pass):
    min_dis = 100
    for i in range(0, 5):
        if (spot_pass[i] == 0 and dist[i] <= min_dis):
            min_dis = dist[i]  
            min_index = i
    return min_index

def path_get(path, j, route):
    if (path[j] == -1):
        return 
    path_get(path, path[j], route)
    print(j)
    route.append(j)

def Node_choose(Node, car_position):
    node, index = find_closest(car_position, Node)
    return index

def direction_decide(Node_cur, Node_nxt, car):
    #v1 = (Node1[0] - car[0], Node1[1] - car[1])
    #v2 = (Node2[0] - car[0], Node2[1] - car[1])
    v1, v2 = vector_cal(Node_cur, Node_nxt, car)
    #print("v1: ", v1)
    #print("v2: ", v2)
    angle = angle_calculation(v1, v2)
    #if (v1[0]*v2[1] - v1[1]*v2[0]) < 0:
    #    angle = -angle
    #print("Angle: ", angle)
    if angle < -35:
        cmd = "right"
    elif angle > 35: 
        cmd = "left"
    else:
        cmd = "straight"
    return cmd

def special_condition(node, target, car, node_idx, lastnode_is_2):
    v1, v2 = vector_cal(node, target, car)
    #print("v1: ", v1)
    #print("v2: ", v2)
    angle = angle_calculation(v1, v2)
    #print("Angle: ", angle)
    cmd = 'none'
    if lastnode_is_2 == 0:
        if (angle < -5 and angle > -60):
            cmd = "straight"
        elif angle <= -60 and angle > -95:
            cmd = "right" 
        elif angle <= -95 and angle >= -120:
            cmd = "right,right,right"
        elif angle < -120:
            cmd = "left,left,left"
        elif angle > 0 and node_idx == 2:
            cmd = "right,left,left"
        elif angle >= -5 and node_idx !=2:
            car_dis = math.sqrt((320 - car[0])**2 + (240 - car[1])**2)
            tar_dis = math.sqrt((320 - target[0])**2 + (240 - target[1])**2)
            if tar_dis > car_dis:
                outside = 0
            else:
                outside = 1
            if outside and angle <= 50:
                cmd = "left,right,right"
            elif outside and angle > 50:
                cmd = "left,right,right,straight"
            elif outside == 0 and angle <= 50:
                cmd = "right,left,left"
            elif outside == 0 and angle > 50:
                cmd = "right,left,left,straight"
    else:
        #if angle >= 60: #and angle <= 120:
        #    cmd = "right,right,straight,right"
        if angle >= 50:
            cmd = "right,right,straight,right"
        elif angle < 50 and angle > 10:
            cmd = "left"
        elif angle <= 10 and angle > -10:
            cmd = "left,right,right"
        elif angle <= -55 and angle > -150:
            cmd = "right" 
        elif -55 < angle and angle <= -30:
            cmd = "left,left,straight,left"
        elif angle > -30 and angle <= 0:
            cmd = "straight"
   
    return cmd


def angle_calculation(v1, v2):
    #cosang = v1[0]*v2[0] +  v1[1]*v2[1]
    #sinang = v1[0]*v2[1] - v1[1]*v2[0]
    Anglev1 = np.arctan2(v1[0], v1[1])*180/np.pi
    Anglev2 = np.arctan2(v2[0], v2[1])*180/np.pi
    if Anglev1 < 0:
        Anglev1 = Anglev1 + 360
    if Anglev2 < 0:
        Anglev2 = Anglev2 + 360   
    if  Anglev2 <= 180 :
        Anglev2 = 180 - Anglev2
    else:
        Anglev2 = 540 - Anglev2
    if Anglev1 <= 180:
        Anglev1 = 180 - Anglev1
    else:
        Anglev1 = 540 - Anglev1
        
    print("1: ", Anglev1)
    print("2: ", Anglev2)
    #angle = (np.arctan2(v2[1], v2[0])- np.arctan2(v1[1], v1[0]))*180/np.pi
    #if (v1[0]*v2[1] - v1[1]*v2[0]) < 0:
    #    angle = -angle
    angle = Anglev2 - Anglev1
    if angle > 180:
       angle = 360 - angle
       angle = -angle
    elif angle < -180 :
       angle = 360 + angle
    print(angle)
    return angle

def vector_cal(pt1, pt2, source):
    v1 = (pt1[1] - source[1], pt1[0] - source[0])
    v2 = (pt2[1] - source[1], pt2[0] - source[0])
    return v1, v2

def information(img, customer = [], car = [], instruction = []):
    text = "Customer:" + ",".join(str(x) for x in customer)
    img_txt = cv2.putText(img, text, (5, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5 ,(0, 0, 255), 1, cv2.LINE_AA)
    text = "Selected_car:" + ",".join(str(x) for x in car)
    img_txt = cv2.putText(img_txt, text, (5, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)  
    text = "Instruction: " + str(instruction)
    img_txt = cv2.putText(img_txt, text, (5, 100), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
    return img_txt


def main(frame, guide_for_path):
    img = mapping(frame)
    #if img is not None:
        #cv2.imshow("frame", img)
        #cv2.waitKey(1)
    cmd = 'none'
    Node_position = ([319, 389], [579, 221], [318, 222], [55, 222], [319, 60])
    if img is not None:
        img_txt = img
        customer = caller_position(img) 
        #taxi = car_position(img)
        taxi = car_calibrate(frame)
        if customer is not None:
            #img_txt = information(img_txt, customer = customer)
            print("Customer Position:", customer)
        if taxi is not None:
            print("Taxi: ", taxi)
            #cv2.circle(img, (taxi[0][0], taxi[0][1]), 8, (255, 255, 0), -1)
            #img_txt = information(img_txt, car = taxi)
            print("Taxi Position: ", taxi)
           # if guide_for_path == 0 and taxi[0][0][0] < 540 and taxi[0][0][0] > 100 and taxi[0][1][0] > 100 and taxi[0][1][0] < 370:
           #     cmd = ("Left")    
      #  for car in taxi:
      #  	cv2.circle(img1, (car[0], car[1]), 10, (0, 0, 255), -1)        
      #  img_car = information(img1, car = taxi)       
      #  cv2.imshow("Test", img_car)
      #  cv2.waitKey(1)
        if len(customer) > 0 and len(taxi) > 0 and guide_for_path == 1:
            arrive = 0
            taxi_selected, number = find_closest(customer, taxi)
            cv2.circle(img, (taxi_selected[0], taxi_selected[1]), 10, (0, 0, 255), -1)
            lastnodeis2 = 0
            #print("Selected car: ", taxi_selected)
            #cv2.imshow("chosen car", img) 
            #cv2.waitKey(1)
            if len(taxi_selected) > 0:
                distance = math.sqrt((customer[0]-taxi_selected[0])**2 + (customer[1] - taxi_selected[1])**2)
                print("Current Target Distance:", distance)
                if distance > 60:
                    target_node = Node_choose(Node_position, customer)
                    car_node = Node_choose(Node_position, taxi_selected)
                    print("Current car Node: ", car_node)
                    print("Current target Node: ", target_node) 
                    if target_node == car_node:
                        if (taxi_selected[0] < 540 and taxi_selected[0] > 100 and taxi_selected[1] > 100 and taxi_selected[1] < 370):
                            lastnodeis2 = 1
                        #print("Last: ",lastnodeis2)
                        cmd = special_condition(Node_position[target_node], customer, taxi_selected, target_node, lastnodeis2)
                        print("Direction: ", cmd)
                    else:
                        nxt_node = path_decide(car_node, target_node)
                        print("Path: ", nxt_node)
                        if len(nxt_node) > 1 and arrive == 0:
                            cmd = direction_decide(Node_position[nxt_node[0]], Node_position[nxt_node[1]], taxi_selected)
                            print("Direction: ", cmd)
                    print("Last:", lastnodeis2)
#                print("Distance: ", math.sqrt((customer[0]-taxi_selected[0])**2 + (customer[1] - taxi_selected[1])**2))i
                       
                    #img_txt = information(img, customer, taxi_selected, cmd)
                    #cv2.imshow("Information", img_txt)
                    #cv2.waitKey(1)
                        
                else:
                    arrive = 1
                    print("Arrive!!!!!!")
                    cmd = "arrive"
            img_txt = information(img, customer, taxi_selected, cmd)
            #gray = cv2.cvtColor(img_txt, cv2.COLOR_BGR2GRAY)
            cv2.imshow("Information", img_txt)
            cv2.waitKey(1)
    return cmd        

camera = cv2.VideoCapture(0)
ser = serial.Serial('/dev/ttyUSB0', 9600)
xbee_pc = XBee(ser) 
   
while True:
#    response = xbee_pc.wait_read_frame()
#    ser.flush()
#    time.sleep(0.1)
#    if response['rf_data'].decode("utf-8") == 'redline':
    ret, frame = camera.read()
    #print(ret)
    guide = 1 
    if ret:
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        #cv2.imshow("HSV", hsv)
        #cv2.waitKey(1)
        cmd = main(frame, guide)
        print("cmd:",cmd)
        #publish.single("eecs/420", cmd, hostname = "192.168.0.3")
        #if cmd != 'none':
        #response = xbee_pc.wait_read_frame()
        #ser.flush()
        #time.sleep(0.1)
        #print("Recieve:",response['rf_data'].decode("utf-8"))
        #if response['rf_data'].decode("utf-8") == 'redline':
        #    print("Send command:", cmd)
        xbee_pc.tx(dest_addr = '\x00\x00', data = cmd)
        ser.flush() 
        time.sleep(0.05)
    if cv2.waitKey(1) & 0XFF == ord('q'):
        break

camera.release()
cv2.destroyAllWindows()






