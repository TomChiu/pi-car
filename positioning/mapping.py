import cv2
import numpy as np
import math

def mapping(frame):
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    #cv2.imshow("hsv", hsv)
    #cv2.waitKey(1)
    lower_white = np.array([80, 120, 160])
    upper_white = np.array([120, 150, 255])
    mask = cv2.inRange(hsv, lower_white, upper_white)
    mask = cv2.dilate(mask, None, iterations = 3)
    mask = cv2.erode(mask, None, iterations = 2)
    #cv2.imshow("mask", mask)
    #cv2.waitKey(1)
    tih, cnts, h = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    #print(len(cnts))
    if len(cnts) > 0 and len(cnts) == 4:
        co = ([0, 0], [0, 0], [0, 0], [0, 0])
        k = 0
        for c in cnts:
            M = cv2.moments(c)
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
            co[k][0] = center[0]
            co[k][1] = center[1]
            k = k + 1
        cols, rows = frame.shape[:2]
        c = sorted(co, key=lambda co: co[:][0])
        #print(c)
        pts1 = np.float32([c[0], c[2], c[1], c[3]])
        pts2 = np.float32([[0, 0], [rows, cols], [0, cols], [rows, 0]])
        matrix = cv2.getPerspectiveTransform(pts1, pts2)
        in_img = cv2.warpPerspective(frame, matrix, (rows,cols))
        #cv2.i00mshow("Perspective Trans", in_img)
        #cv2.waitKey(1)
        return in_img
    else: return 


def car_calibrate(frame):
    cols, rows = frame.shape[:2]
    pts1 = np.float32([[25, 88], [493, 424], [55, 408], [628, 145]])
    pts2 = np.float32([[0, 0], [rows, cols], [0, cols], [rows, 0]])
    matrix = cv2.getPerspectiveTransform(pts1, pts2)
    img = cv2.warpPerspective(frame, matrix, (rows,cols))
    #cv2.imshow("Change", in_img)
    #cv2.waitKey(1)
    #blurred = cv2.GaussianBlur(frame, (11, 11), 0)
    img_cal = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    bluelow = (110, 60, 140)
    bluehigh = (130, 110, 255)
    mask = cv2.inRange(img_cal, bluelow, bluehigh)
    #cv2.imshow("mask", mask)
    #cv2.waitKey(1)
    mask = cv2.erode(mask, None, iterations = 2)
    mask = cv2.dilate(mask, None, iterations = 2)
    im_taxi, cnts, h = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(cnts) > 0:
        car_position = np.zeros((len(cnts), 2), dtype = np.int)
        k = 0
        for i in cnts:
            M = cv2.moments(i)
            if M["m00"] != 0:
                center = [int(M["m10"] / M["m00"]), int((M["m01"] / M["m00"]))] 
                ratio_h = center[0]/640
                ratio_v = 1 - center[1]/480
                #print(ratio_h, ratio_v)
                #print("Untuned center: ", center)
                #car_position[k][0] = center[0]
                #car_position[k][1] = center[1]
                '''
                if (center[0] > 580):
                    car_position[k][0] = center[0] - ratio_h * center[0] * 0.6
                elif (center[0] <= 580 and center[0] > 350):
                    car_position[k][0] = center[0] - ratio_h * center[0] * 0.15
                elif(center[0] <= 350 and center[0] >= 290 and center[1] > 80 and center[1] <=400):
                    car_position[k][0] = center[0] - ratio_h * center[0] * 0.01
                    car_position[k][1] = center[1] - ratio_h * center[1] * 0.35
                elif(center[0] <= 300 and center[0] >= 65): #240
                     car_position[k][0] = center[0] - ratio_h * center[0] * 0.05
                elif(center[0] >= 40 and center[0] < 65):
                     car_position[k][0] = center[0] + ratio_h * center[0] * 0.4 
                elif(center[0] < 40):
                     car_position[k][0] = center[0] + ratio_h * center[0] * 0.75
                '''
                '''
                if (center[1] > 420 and center[1] < 460):
                    car_position[k][1] = center[1] - ratio_v * center[1] * 1.6 
                elif(center[1] >= 460):
                    car_position[k][1] = center[1] - ratio_v * center[1] * 4.2 
                elif(center[1] > 320 and center[1] <= 420):
                    car_position[k][1] = center[1] - ratio_v * center[1] * 0.4
                elif(center[1] <= 320 and center[0] > 350 or center[0] < 300):
                    car_position[k][1] = center[1] - ratio_v * center[1] * 0.5
                '''
                 
                #center[1] = center[1] - ratio_v*center[1]*0.75
                car_position[k][0] = center[0]
                car_position[k][1] = center[1]
                cv2.circle(img, (car_position[k][0], car_position[k][1]), 8, (255, 255, 0), -1)
                k = k + 1
    #car_position = np.array(car_position)
    else:     
        car_position = [] 
    car_position = np.array(car_position)
    return car_position
        #cv2.imshow("car", frame)
if __name__ == '__main__':
    cap = cv2.VideoCapture(-1) 
    while True:
        ret, frame = cap.read() 
        if ret:
            img1 = mapping(frame)
            img2 = car_calibrate(frame)  
        else:
            print("No")
    cap.release()
    cv2.destroyAllWindows()











