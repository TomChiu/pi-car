This folder contains all the code excuting on Raspberry Pi. However, the .h5 file of weights of MobileNet cannot be uploaded due to 10MB upload limits.
To make the car run, type `python3 pi_control.py`
We encourage users to comment out functions about MobileNet in pi_control.py to fine-tune the car first. 
Then, the users can train their own machine learning model to add functions.

The flow in pi_control.py

`cv2.VideoCapture().read()`
-> `process_img()` (include functions from preprocessing.py, draw_line.py, detect_red.py)
-> `predict_sign()` (include functions from getsign.py)
-> `control()` (include functions from cmd.py)
-> if detect red line, `wait_and_turn()` (include functions from communication.py)