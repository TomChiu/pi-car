import numpy as np
import cv2
import glob
from preprocessing import crop

src = np.float32([ [150,  0], [330, 0], [479, 239], [0, 239]])
dst = np.float32([ [  0,  0], [479, 0], [479, 239], [0, 239]])
M = cv2.getPerspectiveTransform(src, dst)
LINE_WIDTH = 230
LINE_NUM = 8
#@profile
def get_lines(img_hls):
	## yellow
	mask_y = check_img_yellow(img_hls)
	mid_x, mid_y = find_pts_of_line(mask_y)
	## white
	mask_w = check_img_white(img_hls)
	mid_x_w, mid_y_w = find_pts_of_line(mask_w, right=True)

	## find yellow line
	if len(mid_y) > LINE_NUM:
		poly_y = np.polyfit(mid_y, mid_x, 2)
		p_y = np.poly1d(poly_y)
	## find white line
	if len(mid_y_w) > LINE_NUM:
		poly_w = np.polyfit(mid_y_w, mid_x_w, 2)
		p_w = np.poly1d(poly_w)			

	FAIL_ALL = (len(mid_y) <= LINE_NUM and len(mid_y_w) <= LINE_NUM)
	FAIL_W = len(mid_y_w) <= LINE_NUM 
	FAIL_Y = len(mid_y) <= LINE_NUM

	## get mid line	
	if FAIL_ALL: 
		print('NO LINE')
		return False, -1
	elif FAIL_W:
		print('YELLOW ONLY')
		CASE = 1
	elif FAIL_Y:
		print('WHITE ONLY')
		CASE = 2
	else:
		print('BOTH')
		CASE = 0

	if CASE == 0:
		mid_poly = get_mid_poly(p_y, p_w)
	elif CASE == 1:
		mid_poly = poly_y
		mid_poly[-1] += LINE_WIDTH
	elif CASE == 2:
		mid_poly = poly_w
		mid_poly[-1] -= LINE_WIDTH
	## polynomial of mid line
	p_mid = np.poly1d(mid_poly)
	
	return True, p_mid

def check_img_yellow(img_hls):
	lower_yellow = np.uint8([10,  0, 40])
	upper_yellow = np.uint8([45, 255, 255]) 
	
	mask_y = cv2.inRange(img_hls, lower_yellow, upper_yellow)
	return mask_y

def check_img_white(img_hls):
	lower_white = np.uint8([  0, 180,  0])
	upper_white = np.uint8([255, 255, 255]) 
	
	mask_w = cv2.inRange(img_hls, lower_white, upper_white)
	return mask_w

def find_pts_of_line(mask, length=5, step=15, right=True):
	mid_x, mid_y = [], []
	for c in range(0, mask.shape[0], step):
		nonzeros = np.nonzero(mask[c, :])[0]
		if len(nonzeros):
			if right:
				avg = np.mean(nonzeros)
				nonzeros = nonzeros[nonzeros > avg]
			if len(nonzeros) > mask.shape[1] / 6:
				for i in range(0, len(nonzeros), step):
					mid_y.append(c)
					mid_x.append(nonzeros[i])
			elif len(nonzeros) > length:
				for i in range(0, len(nonzeros)-length, length):
					if nonzeros[i] + length == nonzeros[i+length]:
						mid_y.append(c)
						mid_x.append(nonzeros[i]+length)
						break
	
	return mid_x, mid_y

def get_mid_poly(p_y, p_w, step=10):
	mid_x, mid_y = [], []
	for i in range(0, 240, step):
		x = (p_y(i) + p_w(i)) / 2
		mid_x.append(x)
		mid_y.append(i)
	mid_poly = np.polyfit(mid_y, mid_x, 2)
	return mid_poly

def find_ctl_pts(p_mid, num=10):
	pts = []
	step = -int(220/num)
	for y in range(239, 59, step):
		x = int(p_mid(y))
		if x < 0: break
		pts.append((x, y))
	if len(pts) < 3:	done = False
	else:				done = True
	return done, pts

def draw_pts(img_size, pts):
	img_pts = np.zeros((img_size[0], img_size[1]), dtype=np.uint8)
	for x, y in pts:
		cv2.circle(img_pts, (x, y), 3, 255, -1, 8, 0)
	return img_pts

def calibrate(img):
	img_resized = cv2.warpPerspective(img, M, (img.shape[1], img.shape[0]), flags=cv2.INTER_NEAREST)
	return img_resized

#@profile
def main():
	filenames = glob.glob('./img/*.jpg')
	for f in filenames:
		img = cv2.imread(f)
		img_resized = crop(img)	# resize and crop
		np.polyfit([0, 1, 2, 3], [0, 1, 4, 9], 2)
#		cv2.imshow('img_resized', img_resized)
#		cv2.waitKey()
		img_resized = cv2.warpPerspective(img_resized, M, (img_resized.shape[1], img_resized.shape[0]), flags=cv2.INTER_NEAREST)

		img_hls = cv2.cvtColor(img_resized, cv2.COLOR_BGR2HLS)
#		cv2.imshow('img_lines', img_hls)
#		cv2.waitKey()
		done, p_mid = get_lines(img_hls)
		if done:
			done, pts = find_ctl_pts(p_mid)
			img_pts = draw_pts(img_hls.shape, pts)
			cv2.imshow('img_pts', img_pts)
			cv2.waitKey()
		cv2.destroyAllWindows()
		break
if __name__ == '__main__':
	main()
