from xbee import XBee
import serial
import time

ser = serial.Serial('/dev/ttyS0', 9600)
xbee = XBee(ser)

def receive():
	ser.flushInput()
	r = xbee.wait_read_frame()
	r = r['rf_data'].decode("utf-8").split(",")
	return r

def check_arr():
	r = xbee.wait_read_frame()
	ser.flush()
	r = r['rf_data'].decode("utf-8").split(",")
	return r


if __name__ == '__main__':
	while 1:
		print(receive())
