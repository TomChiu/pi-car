import numpy as np
import cv2
from glob import glob
from draw_line import get_lines

def detect_red(img, p_mid, to_show=False):
	img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
	mask = find_red_mask(img_hsv)

	x, y = find_pts_of_red_line(mask)
	done_line, y_dist = check_valid_line(x, y, p_mid)
	
	if to_show:
		gray = cv2.cvtColor(img_hsv, cv2.COLOR_BGR2GRAY)
		for i in range(len(x)):
			cv2.circle(gray, (x[i], y[i]), 3, 255)
		cv2.imshow('gray', gray)
		cv2.waitKey(1)
	if done_line:	return True, (img_hsv.shape[0] - y_dist)
	else:			return False, -1

def find_red_mask(img_hsv):
	lower_red = np.array([  0,  70,  50])
	upper_red = np.array([ 10, 255, 255])
	mask = cv2.inRange(img_hsv, lower_red, upper_red)
	lower_red = np.array([ 170,  70,  50])
	upper_red = np.array([ 180, 255, 255])
	mask2 = cv2.inRange(img_hsv, lower_red, upper_red)
	mask = cv2.bitwise_or(mask, mask2)
	
	return mask

def find_pts_of_red_line(mask, length=5, step=20):
	x, y = [], []
#	last_x, last_y = None, None
	for r in range(0, mask.shape[1], step):
		nonzeros = np.nonzero(mask[:, r])[0]
		if len(nonzeros) > length:
			for i in range(0, len(nonzeros)-length, length):
				if nonzeros[i] + length == nonzeros[i+length]:
					x.append(r)
					y.append(nonzeros[i])
					break
	return x, y

def check_valid_line(x, y, p_mid):
	if len(x) < 4: 
#		print('not enough points')
		return False, -1
	valid_list = abs(x - np.mean(x)) < 1 * np.std(x)
	x = np.array(x)[valid_list]
	y = np.array(y)[valid_list]
	last_y = y[0]
	for i in range(1, len(y)):
		if abs(last_y - y[i]) > 10: 
#			print('unstable line')
			return False, -1
		last_y = y[i]
	p = np.poly1d(p_mid)
	lane = p(y[0])
	y_dist = y[0]
	if lane > np.max(x) or lane < np.min(x):
#		print('not on the lane')
		return False, -1
	else:
		return True, y_dist

def img_show(img):
	cv2.imshow('img', img)
	cv2.waitKey()

if __name__ == '__main__':
	f = glob('./img/*.jpg')
	for filename in f:
		img = cv2.imread(filename)
		img_hls = cv2.cvtColor(img, cv2.COLOR_BGR2HLS)
		done, p_mid = get_lines(img_hls)
		ans = detect_red(img, p_mid, to_show=False)
		print(ans)
		img_show(img)
