'''
for recognizing the sign by mobilenet 
11/26 2018
'''
import warnings
warnings.filterwarnings('ignore')
import keras
from keras.preprocessing import image
from keras.applications.mobilenet import preprocess_input
import numpy as np
import serial
import sys, tty, termios
import cv2
from keras.models import load_model
from keras.utils.generic_utils import CustomObjectScope

#s = serial.Serial("/dev/ttyACM0")

result = {'0':'stop', '1':'20', '2':'50', '3':'100', '4':'back', '5':'forward', '6':'left', '7':'right', '8':'none'}
 
def img2input(s=None):
    x = image.img_to_array(s)
    x = np.expand_dims(x, axis=0)
    return x

def makered():
    red = np.zeros((32, 32, 3) , np.uint8)
    red[:, :] = (0, 0, 255)
    return red

def getsign(img=None):
    #img = cv2.resize(img, (960, 640), interpolation=cv2.INTER_CUBIC)
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    #cv2.imshow('hsv_frame', hsv_img)
    #cv2.waitKey(1)
    low_range = np.array([0, 150, 100])
    high_range = np.array([40, 255, 200])
    th = cv2.inRange(hsv_img, low_range, high_range)
    th, cnts, h = cv2.findContours(th, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if(len(cnts)==0):
        return makered(), 0
    cmax = max(cnts, key = cv2.contourArea)
    corner = cv2.approxPolyDP(cmax, 30, True)
    if(len(corner)!=4):
        return makered(), 0
    co = ([0,0],[0,0],[0,0],[0,0])
    co[0][0] = corner[0][0][0]
    co[0][1] = corner[0][0][1]
    co[1][0] = corner[1][0][0]
    co[1][1] = corner[1][0][1]
    co[2][0] = corner[2][0][0]
    co[2][1] = corner[2][0][1]
    co[3][0] = corner[3][0][0]
    co[3][1] = corner[3][0][1]
    c = sorted(co, key=lambda co: co[:][0])
    if(c[0][1] >= c[1][1]):
        tmp = c[1]
        c[1] = c[0]
        c[0] = tmp
    if(c[2][1] >= c[3][1]):
        tmp = c[3]
        c[3] = c [0]
        c[2] = tmp
    pts1 = np.float32([c[0], c[2], c[1], c[3]])
    pts2 = np.float32([[0, 0], [32, 0], [0, 32], [32, 32]])
    matrix = cv2.getPerspectiveTransform(pts1, pts2)
    sign = cv2.warpPerspective(img, matrix, (32,32))
    return sign, 1

def setup(mname = None, cam =-1):
    vc = cv2.VideoCapture(cam)
    #result = {'0':'stop', '1':'20', '2':'50', '3':'100', '4':'back', '5':'forward', '6':'left', '7':'right', '8':'none'}
    with CustomObjectScope({'relu6': keras.applications.mobilenet.relu6, 'DepthwiseConv2D':
        keras.applications.mobilenet.DepthwiseConv2D}):
        model = load_model(mname)
    print("model loaded..")
    a = model.predict(img2input(makered()))
    print("warming up finished..")
    return model, vc

def predict_sign(img, model):
    sign, key = getsign(img)
    if(key==1):
        cv2.imshow('frame', sign)
        cv2.waitKey(1)
        x = img2input(sign)	
        images = np.vstack([x])
        classes = model.predict_classes(images, batch_size=1)
        ans = result[str(classes[0])]
        return ans
    else:
        cv2.imshow('frame', sign)
        cv2.waitKey(1)
        return 'none'

def main():
    model, vc = setup('model.h5', cam=-1)

    while 1:
        ret, img = vc.read()
        ans = predict_sign(img, model)
        print(ans)
    vc.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
