import numpy as np
import serial
import time

SLOW = 0
AVG = 1
FAST = 2

REF_RIGHT = 115
REF_LEFT = 150

def _control_center(MODE, margin):
	if abs(margin) > 150: 
		if margin < 0: margin = -140
		else: margin = 140
	turn = 1
	if abs(margin) > 20: turn = (150 - abs(margin)) / 150
	if margin < 0: turn = -turn
	if abs(turn) < 0.1:
		if turn > 0: turn = 0.08
		else: turn = -0.08
	if MODE == SLOW:
		speed = int(32+10*abs(turn))
	elif MODE == AVG:
		speed = int(35+15*abs(turn))
	elif MODE == FAST:
		speed = int(38+18*abs(turn))
	turn = -turn
	if abs(turn + 1) < 0.05: turn = 1
	return speed, turn

def send_cmd(MODE, margin, s):
	speed, turn = _control_center(MODE, margin)
	cmd = '/ServoTurn/run ' + str(speed) + ' ' + '{0:.2f}'.format(turn) + ' \n'
	print(cmd)
	s.write(cmd.encode())

def go_left(s, dist):
	'''
	wait_time_right = np.around(dist/REF_LEFT, decimals=2)
	cmd = '/ServoTurn/run 30 1.00 \n'
	s.write(cmd.encode())
	print(cmd)
	time.sleep(wait_time_right)
	'''

	cmd = '/ServoLeft/run \n'
	s.write(cmd.encode())
	print(cmd)
	time.sleep(7.1)

def go_right(s, dist):
	'''
	wait_time_right = np.around(dist/REF_RIGHT, decimals=2)
	cmd = '/ServoTurn/run 30 1.00 \n'
	s.write(cmd.encode())
	print(cmd)
	time.sleep(wait_time_right)
	'''
	
	cmd = '/ServoRight/run \n'
	s.write(cmd.encode())
	print(cmd)
	time.sleep(4.15)	
	
def go_straight(s):
	cmd = '/ServoTurn/run 50 1.00 \n'
	s.write(cmd.encode())
	print(cmd)
	time.sleep(2.3)

if __name__ == '__main__':
	while 1 :
		s = serial.Serial('/dev/ttyACM0')
		cmd = '/ServoTurn/run 50 0.005 \n'
		s.write(cmd.encode())
		print(cmd)
		time.sleep(5)
