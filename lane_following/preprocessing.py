import numpy as np
import cv2
import glob

def crop(img):
	half_w = int(img.shape[1]/8)
	half_h = int(img.shape[0]/4)
	img_resized = img[half_h:img.shape[0]-half_h, half_w:img.shape[1]-half_w, :]

	return img_resized
