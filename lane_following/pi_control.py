import sys
import time
import cv2
import numpy as np
import serial
import glob
from preprocessing import crop
from draw_line import get_lines, find_ctl_pts, calibrate, draw_pts 
from detect_red import detect_red
from getsign import setup, predict_sign
from get_area import getsignarea
from cmd import send_cmd, go_left, go_right, go_straight
from utils import find_pt, filter_class, filter_ans, set_mode
from communication import receive, check_arr

REF = 140		# reference distance after detecting th red line
TO_SHOW = False # streaming on terminal or not

buf_dir = []

SLOW = 0		# define different modes
AVG = 1
FAST = 2

def process_img(img):
	img_resized = crop(img)
	img_resized = calibrate(img_resized)
	img_hls = cv2.cvtColor(img_resized, cv2.COLOR_BGR2HLS)
	done, p_mid = get_lines(img_hls)
	if done:
		done, pts = find_ctl_pts(p_mid, 15)	# decide how many points on the line
		pts = pts[10:12]					# pick certain points of them
		img_pts = draw_pts(img_hls.shape, pts)
		ans, dist = detect_red(img_resized, p_mid)
		return done, pts, p_mid, img_pts, ans, dist
	else:	return False, -1, -1, -1, -1, -1

def control(pts_buf, pts, img_pts, MODE, s):
	if not pts: return pts_buf
	if not pts_buf: pts_buf = pts
	## calculate control points
	d = 10
	# filter new points
	if abs(pts[-1][1] - pts_buf[-1][1]) < d:
		pts_buf.append(pts[-1])
	control_pt = find_pt(pts_buf)
	margin = control_pt[0] -  int(img_pts.shape[1] / 2)
	pts_buf.pop(0)

	## command the car
	send_cmd(MODE, margin, s)

	return pts_buf

def wait_and_turn(next_turn, dist, s):
	global buf_dir
	### keep going for a while
	wait_time = np.around(dist/REF, decimals=2)
	cmd = '/ServoTurn/run 60 1.00 \n'
	s.write(cmd.encode())
	print(cmd)
	time.sleep(wait_time)

	### stop first
	cmd = '/ServoStop/run \n'
	s.write(cmd.encode())
	print(cmd)
	print('stop and wait')
	time.sleep(1)

	if len(buf_dir) == 0:
		response = receive()
		buf_dir = response
		ans = buf_dir.pop(0)
	else:
		ans = buf_dir.pop(0)
	
	if ans == 'left':
		print('PC said go left')
		go_left(s, dist)
	elif ans == 'right':
		print('PC said go right')
		go_right(s, dist)
	elif ans == 'straight':
		print('PC said go straight')
		go_straight(s)		
	else:
		if next_turn == None:
			print('nothing from PC, go straight')
			go_straight(s)
		elif next_turn == 'left':
			print('nothing from PC, sign said go left')
			go_left(s)
		elif next_turn == 'forward':
			print('nothing from PC, sign said go forward')
			go_straight(s)
		elif next_turn == 'right':
			print('nothing from PC, sign said go right')
			go_right(s, dist)
	print(buf_dir)

def main():
	s = serial.Serial('/dev/ttyACM0')
	FPS = 5
	### camera 1 for lane_following
	camera1 = cv2.VideoCapture(0)
	camera1.set(cv2.CAP_PROP_FPS, FPS)
	### camera 2 for MobileNet
	model, camera2 = setup('model.h5', cam=1)	
	camera2.set(cv2.CAP_PROP_FPS, FPS)
	###
	for i in range(0, FPS*5): 
		camera1.read()
		camera2.read()
	start = time.time()
	
	### INIT
	MODE = FAST
	pts_buf = []
	class_buf = []
	ans_buf = []
	action = None
	next_turn = None
	last_arrive_time = time.time()
	count = 0
	###
	while 1:
		count += 1
		done = 0
		ret1, img = camera1.read()
		ret2, img_net = camera2.read()
		### image processing for lane following
		if ret1:
			done, pts, p_mid, img_pts, ans, dist = process_img(img)
		t = time.time()
		print('%.2f' % (t - start))	# passed time
		if done:
			if TO_SHOW:
				### for image show
				img = crop(img)
				img = calibrate(img)
				img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
				img_pts = cv2.addWeighted(img_gray, 1, img_pts, 1, 0)
				img_pts_show = cv2.resize(img_pts, (160, 120))
				### 
			sign_class = predict_sign(img_net, model)
			print(sign_class)
			if sign_class != 'none' or class_buf:
				class_buf.append(sign_class)
			action, class_buf = filter_class(class_buf)
			print(action)
#			action = 'none' #if add mobilenet, delete this
			if action != 'none':
				next_turn = action
				print(action)
			###
			if ans != False or ans_buf:
				ans_buf.append(ans)
			stop, ans_buf = filter_ans(ans_buf)
			### lane following
			MODE = set_mode(MODE, action)
			pts_buf = control(pts_buf, pts, img_pts, MODE, s)
			if check_arr()[0] == 'arrive' and time.time() - last_arrive_time > 10:
				print('ARRIVE!!!!')
				s.write('/ServoStop/run \n'.encode())
				time.sleep(10)
				last_arrive_time = time.time()
			if stop:
				next_turn = wait_and_turn(next_turn, dist, s)
				pts_buf = []
		else:
			if TO_SHOW:
				### for image show
				img = crop(img)
				img = calibrate(img)
				img_pts_show = cv2.resize(img, (160, 120))
				img_pts_show = cv2.cvtColor(img_pts_show, cv2.COLOR_BGR2GRAY)
				###
		if TO_SHOW:
			cv2.imshow('img', img_pts_show)
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break

if __name__ == '__main__':	
	main()
