import cv2
import numpy as np

cap = cv2.VideoCapture(-1)

def getsignarea(img=None):
    img = cv2.resize(img, (480, 320), interpolation=cv2.INTER_CUBIC)
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    low_range = np.array([0, 160, 80])
    high_range = np.array([40, 255, 160])
    th = cv2.inRange(hsv_img, low_range, high_range)
    th, cnts, h = cv2.findContours(th, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cv2.imshow('con', th)
    cv2.waitKey(1)
    if(len(cnts)==0):
        return 0
    cmax = max(cnts, key = cv2.contourArea)
    corner = cv2.approxPolyDP(cmax, 30, True)
    if(len(corner)!=4):
        return 0
    return cv2.contourArea(cmax)

def main():
    while(True):
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.resize(gray, (120, 80), interpolation=cv2.INTER_CUBIC)
        
        cv2.imshow('frame', gray)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        
        print(getsignarea(frame))

if __name__ == '__main__':
    main()
cap.release()
cv2.destroyAllWindows()

