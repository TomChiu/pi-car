# Rapberry Pi Self-Driving Car

We have implemented a self-driving car with Rapberry Pi.
* Servo Motor Control:      K64F Microcontroller, RPC call
* Lane Following:           Image Processing
* Positioning:              Dijkstra's Shortest Path Algorithm
* Deep Learning:            Self-Trained MobileNet
* Wireless Communication:   XBEE S2C

Demo Videos can be found here: <http://m104.nthu.edu.tw/~s104061211/>