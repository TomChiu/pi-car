sudo apt-get update && sudo apt-get upgrade
sudo apt install build-essential cmake pkg-config
sudo apt install libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev
sudo apt install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libxvidcore-dev libx264-dev
sudo apt install libgtk2.0-dev libgtk-3-dev
sudo apt install libatlas-base-dev gfortran
sudo apt install python3-dev python3-pip
sudo apt install ffmpeg gstreamer1.0* libcanberra-gtk* libpng-dev zlib1g-dev libavresample-dev libgstreamer-plugins-base1.0-dev gphoto2 libgphoto2* libdc1394-22 libdc1394-22-dev libgstreamer0.10-dev libopenblas-dev libgflags-dev libgoogle-glog-dev libprotobuf-dev
sudo apt-get install vim
pip3 install matplotlib
pip3 install sklearn scikit-image
pip3 install opencv-python
pip3 install setuptools wheel
pip3 install tqdm
pip3 install serial
pip3 install tensorflow
pip3 install keras

