#ifndef BBCAR_H
#define BBCAR_H
#include "parallax_servo.h"

extern float global_kp, global_ki;
void bbcar_init( PwmOut &pin_servo0, PwmOut &pin_servo1 );
void ServoStop( int speed );
void ServoCtrl( int speed );
void ServoTurn( int speed, double turn );
void ServoRight();
void ServoLeft();
void Buzz();
void controller( float err );
void setController( float kp, float ki );

#endif
